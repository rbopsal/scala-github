package io.ase.scalac.server.tasks

import java.time.Instant
import java.util.concurrent.StructuredTaskScope
import java.util.concurrent.StructuredTaskScope.Subtask
import scala.collection.immutable.VectorBuilder
import scala.concurrent.duration.*
import scala.quoted.*
import scala.util.{Success, Try, Using}


object TaskMacros {

  type ParallelCtx[T, R] = StructuredTaskScope[T] ?=> VectorBuilder[Subtask[T]] ?=> R
  type     RaceCtx[T, R] = StructuredTaskScope.ShutdownOnSuccess[T] ?=> R
  type  SuccessCtx[T, R] = StructuredTaskScope.ShutdownOnFailure ?=> VectorBuilder[Subtask[T]] ?=> R

  private def joinTypeImpl[A: Type, T: Type](a: Expr[A], scope: Expr[StructuredTaskScope[T]])(using Quotes): Expr[StructuredTaskScope[T]] = {
    a match {
      case '{ $i: Instant  } => '{ ${ scope }.joinUntil($i) }
      case '{ $d: Duration } => '{ ${ scope }.joinUntil(Instant.now().plusNanos($d.toNanos)) }
      case _ => '{ ${ scope }.join() }
    }
  }

  def parallelTasksImpl[A: Type, T: Type](a: Expr[A], block: Expr[ParallelCtx[T, Unit]])(using Quotes): Expr[Try[Vector[Try[T]]]] = '{
    Using(StructuredTaskScope[T]) { scope =>
      val vb = VectorBuilder[Subtask[T]]
      given VectorBuilder[Subtask[T]] = vb
      given StructuredTaskScope[T] = scope

      ${ block }
      ${ joinTypeImpl(a, '{ scope }) }

      // Some, or all of the results, could have failed.
      vb.result().map(st => Try(st.get()))
    }
  }

  def raceTasksImpl[A: Type, T: Type](a: Expr[A], block: Expr[RaceCtx[T, Unit]])(using Quotes): Expr[Try[T]] = '{
    Using(StructuredTaskScope.ShutdownOnSuccess[T]) { scope =>
      given StructuredTaskScope.ShutdownOnSuccess[T] = scope

      ${ block }
      ${ joinTypeImpl(a, '{ scope }) }

      // Return first successful result, or, if none, a fail. 
      Try(scope.result())
    }.flatten
  }

  def successTasksImpl[A: Type, T: Type](a: Expr[A], block: Expr[SuccessCtx[T, Unit]])(using Quotes): Expr[Try[Vector[T]]] = '{
    Using(StructuredTaskScope.ShutdownOnFailure()) { scope =>
      val vb = VectorBuilder[Subtask[T]]
      given VectorBuilder[Subtask[T]] = vb
      given StructuredTaskScope.ShutdownOnFailure = scope

      ${ block }
      ${ joinTypeImpl(a, '{ scope }) }
      scope.throwIfFailed()

      // Once here, all results have been successful.  Transform Vector[Try[T]] to Try[Vector[T]].
      Success(vb.result().map(st => st.get()))
    }.flatten
  }
}