package io.ase.scalac.server.tasks

import TaskMacros.*
import java.time.Instant
import java.util.concurrent.StructuredTaskScope.Subtask
import java.util.concurrent.{Callable, CompletableFuture, Executors, ExecutorService, StructuredTaskScope}
import scala.collection.immutable.VectorBuilder
import scala.concurrent.duration.*
import scala.concurrent.{ExecutionContext, Future}
import scala.jdk.FutureConverters.CompletionStageOps
import scala.quoted.*
import scala.util.{Try, Using}


///
// `Tasks` classes evaluate only when a `run`, `runF`, or `runCF` method is invoked.
// `Tasks` classes can be invoked multiple times.
//
//    `T` is the out value type.
//    `R` is the out wrapper type.
//
// For objects utilizing the Tasks trait (and virtual threads), suggested usage is:
//
//    given ExecutionContext = Tasks.exctx
//
//      or
//
//    import Tasks.exctx
//
// Rather than the global version:
//
//    given ExecutionContext = ExecutionContext.Implicits.global
///

object Tasks {
  
  given exsrv: ExecutorService  = Executors.newVirtualThreadPerTaskExecutor()
  given exctx: ExecutionContext = ExecutionContext.fromExecutor(exsrv)

  // Matching Scala's Future `apply` signature.
  def apply[T](block: => T): ExecutionContext ?=> Future[T] = CompletableFuture.supplyAsync(() => block, exsrv).asScala

  private given tryToCompletableFuture[T, R[_]]: Conversion[Try[R[T]], CompletableFuture[R[T]]] = `try` => {
    val cf = CompletableFuture[R[T]]()
    `try`.fold(cf.completeExceptionally(_), cf.complete(_))
    cf
  }
}

///
//  `run`   => Wait on `Tasks` started to complete successfully or exceptionally wrapped as a Scala Try.
//  `runF`  => Wrap the `run` method as a Scala Future.
//  `runCF` => Wrap the `run` method as a Java CompletableFuture.
///

trait Tasks[T, R[_]] {
  
  def run              : Try[R[T]]
  def run(d: Duration) : Try[R[T]]
  def run(i: Instant ) : Try[R[T]]

  def runF             : ExecutionContext ?=> Future[R[T]] = runCF.asScala
  def runF(d: Duration): ExecutionContext ?=> Future[R[T]] = runCF(d).asScala
  def runF(i: Instant ): ExecutionContext ?=> Future[R[T]] = runCF(i).asScala

  def runCF: CompletableFuture[R[T]] = {
    CompletableFuture.supplyAsync(() => run, Tasks.exsrv).thenCompose(Tasks.tryToCompletableFuture(_))
  }

  def runCF(d: Duration): CompletableFuture[R[T]] = {
    CompletableFuture.supplyAsync(() => run(d), Tasks.exsrv).thenCompose(Tasks.tryToCompletableFuture(_))
  }

  def runCF(i: Instant): CompletableFuture[R[T]] = {
    CompletableFuture.supplyAsync(() => run(i), Tasks.exsrv).thenCompose(Tasks.tryToCompletableFuture(_))
  }
}


///
// A single task based a Java virtual thread and a CompletableFuture.
// Class `Task` can be invoked multiple times.
///

object Task {
  extension (i: Instant) private def toDuration = java.time.Duration.between(Instant.now(), i).abs()
  type C[T] = T
}

class Task[T](block: => T) extends Tasks[T, Task.C] {
  import Task.*

  def run: Try[T] = {
    Try(CompletableFuture.supplyAsync(() => block, Tasks.exsrv).join())
  }

  def run(d: Duration): Try[T] = {
    Try(CompletableFuture.supplyAsync(() => block, Tasks.exsrv).orTimeout(d.toNanos, NANOSECONDS).join())
  }

  def run(i: Instant): Try[T] = {
    Try(CompletableFuture.supplyAsync(() => block, Tasks.exsrv).orTimeout(i.toDuration.toNanos, NANOSECONDS).join())
  }

  override def runCF: CompletableFuture[T] = {
    CompletableFuture.supplyAsync(() => block, Tasks.exsrv)
  }

  override def runCF(d: Duration): CompletableFuture[T] = {
    CompletableFuture.supplyAsync(() => block, Tasks.exsrv).orTimeout(d.toNanos, NANOSECONDS)
  }

  override def runCF(i: Instant): CompletableFuture[T] = {
    CompletableFuture.supplyAsync(() => block, Tasks.exsrv).orTimeout(i.toDuration.toNanos, NANOSECONDS)
  }
}


///
//  Start a group of parallel tasks (as virtual threads) and wait on the `join`.
//  A `join` means wait on forked tasks to succeed or fail.
//  After the `join`, the tasks can be processed by their `Success` or `Failure` state.
//
//  These routines don't stop evaluating tasks if one or more tasks fail.
//
//  In the passed `block` variable, wrap `Callable` functions with `task`.  This
//  indicates a virtual thread invocation.
//
//  The returned type is of Try[Vector[Try[T]]].
///

object ParallelTasks {
  def task[A <: T, T](call: Callable[A]): ParallelCtx[T, Subtask[T]] = {
    val vb = summon[VectorBuilder[Subtask[T]]]
    val scope = summon[StructuredTaskScope[T]]
    val st: Subtask[T] = scope.fork(call)
    vb.addOne(st)
    st
  }

  def task[A <: T, T](f: => A): ParallelCtx[T, Subtask[T]] = {
    val call = new Callable[A] { def call = f }
    task(call)
  }

  inline def parallelTasks[T](block: => ParallelCtx[T, Unit]): Try[Vector[Try[T]]] = {
    ${ parallelTasksImpl('{ () }, '{ block }) }
  }

  inline def parallelTasksFor[T](d: Duration)(block: => ParallelCtx[T, Unit]): Try[Vector[Try[T]]] = {
    ${ parallelTasksImpl('{ d }, '{ block }) }
  }

  inline def parallelTasksUntil[T](i: Instant)(block: => ParallelCtx[T, Unit]): Try[Vector[Try[T]]] =
    ${ parallelTasksImpl('{ i }, '{ block }) }

  // Filter out (remove) `Failure[T]` from results.
  given filterSuccess[T]: Conversion[Try[Vector[Try[T]]], Try[Vector[T]]] = _.map( _.filter(_.isSuccess).map(_.get) )

  // See Functional Programming in Scala, 2nd., Chapter 7, `parMap`.
  def parMap[I, T](items: Seq[I])(f: I => T): ParallelTasks[T] = ParallelTasks[T] {
    items.foreach(item => task(f(item)))
  }

  def parMap2[I0, I1, T](i0s: Seq[I0], i1s: Seq[I1])(f: (I0, I1) => T): ParallelTasks[T] = ParallelTasks[T] {
    parMap(i0s.zip(i1s))(f.tupled)
  }

  type VTC[T] = Vector[Try[T]]
}

class ParallelTasks[T](block: => ParallelCtx[T, Unit]) extends Tasks[T, ParallelTasks.VTC] {
  import ParallelTasks.*
  inline def run = parallelTasks(block)
  inline def run(d: Duration) = parallelTasksFor(d)(block)
  inline def run(i: Instant)  = parallelTasksUntil(i)(block)
}


///
//  Start a group of parallel tasks (as virtual threads) and wait on the `join`.
//  A `join` means wait on forked tasks; returning the first to succeed.
//  Once a task has successfully completed, other tasks are interrupted.
//
//  In the passed `block` variable, wrap `Callable` functions with `task`.  This
//  indicates a virtual thread invocation.
//
//  The returned type is of Try[T].
///

object RaceTasks {
  def task[A <: T, T](call: Callable[A]): RaceCtx[T, Subtask[T]] = {
    val scope = summon[StructuredTaskScope[T]]
    scope.fork(call)
  }

  def task[A <: T, T](f: => A): RaceCtx[T, Subtask[T]] = {
    val call = new Callable[A] { def call = f }
    task(call)
  }

  inline def raceTasks[T](block: => RaceCtx[T, Unit]): Try[T] = {
    ${ raceTasksImpl('{ () }, '{ block }) }
  }

  inline def raceTasksFor[T](d: Duration)(block: => RaceCtx[T, Unit]): Try[T] = {
    ${ raceTasksImpl('{ d }, '{ block }) }
  }

  inline def raceTasksUntil[T](i: Instant)(block: => RaceCtx[T, Unit]): Try[T] = {
    ${ raceTasksImpl('{ i }, '{ block }) }
  }

  // See Functional Programming in Scala, 2nd., Chapter 7, `parMap`.
  def parMap[I, T](items: Seq[I])(f: I => T): RaceTasks[T] = RaceTasks[T] {
    items.foreach(item => task(f(item)))
  }

  def parMap2[I0, I1, T](i0s: Seq[I0], i1s: Seq[I1])(f: (I0, I1) => T): RaceTasks[T] = RaceTasks[T] {
    parMap(i0s.zip(i1s))(f.tupled)
  }

  type C[T] = T
}

class RaceTasks[T](block: => RaceCtx[T, Unit]) extends Tasks[T, RaceTasks.C] {
  import RaceTasks.*
  inline def run = raceTasks(block)
  inline def run(d: Duration) = raceTasksFor(d)(block)
  inline def run(i: Instant)  = raceTasksUntil(i)(block)
}


///
//  Start a group of parallel tasks (as virtual threads) and wait on the `join`.
//  A `join` waits on forked tasks and returns immediately on error or waits on all tasks
//  to successfully complete.
//
//  In the passed `block` variable, wrap `Callable` functions with `task`.  This
//  indicates a virtual thread invocation.
//
//  The returned type is of Try[Vector[T]].
///

object SuccessTasks {
  def task[A <: T, T](call: Callable[A]): SuccessCtx[T, Subtask[T]] = {
    val vb = summon[VectorBuilder[Subtask[T]]]
    val scope = summon[StructuredTaskScope.ShutdownOnFailure]
    val st: Subtask[T] = scope.fork(call)
    vb.addOne(st)
    st
  }

  def task[A <: T, T](f: => A): SuccessCtx[T, Subtask[T]] = {
    val call = new Callable[A] { def call = f }
    task(call)
  }

  inline def successTasks[T](block: => SuccessCtx[T, Unit]): Try[Vector[T]] = {
    ${ successTasksImpl('{ () }, '{ block }) }
  }

  inline def successTasksFor[T](d: Duration)(block: => SuccessCtx[T, Unit]): Try[Vector[T]] = {
    ${ successTasksImpl('{ d }, '{ block }) }
  }

  inline def successTasksUntil[T](i: Instant)(block: => SuccessCtx[T, Unit]): Try[Vector[T]] = {
    ${ successTasksImpl('{ i }, '{ block }) }
  }

  // See Functional Programming in Scala, 2nd., Chapter 7, `parMap`.
  def parMap[I, T](items: Seq[I])(f: I => T): SuccessTasks[T] = SuccessTasks[T] {
    items.foreach(item => task(f(item)))
  }

  def parMap2[I0, I1, T](i0s: Seq[I0], i1s: Seq[I1])(f: (I0, I1) => T): SuccessTasks[T] = SuccessTasks[T] {
    parMap(i0s.zip(i1s))(f.tupled)
  }

  type VC[T] = Vector[T]
}

class SuccessTasks[T](block: => SuccessCtx[T, Unit]) extends Tasks[T, SuccessTasks.VC] {
  import SuccessTasks.*
  inline def run = successTasks(block)
  inline def run(d: Duration) = successTasksFor(d)(block)
  inline def run(i: Instant)  = successTasksUntil(i)(block)
}