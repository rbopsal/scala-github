///
// Copyright 2024 by Adron Systems LLC, Laporte, MN  56461.
// All rights reserved.
//
//   Purpose: Contributor definition plus Argonaut conversions.
///

package io.ase.scalac.server.entities

import io.circe.*
import cats.Applicative
import cats.effect.Concurrent
import cats.implicits.*
import org.http4s.{EntityDecoder, EntityEncoder, circe}

object Contributors {

  case class Contributor(name: String, contributions: Int)

  given Encoder[Contributor] = new Encoder[Contributor] {
    final def apply(item: Contributor): Json = Json.obj(
      ("name" -> Json.fromString(item.name)),
      ("contributions" -> Json.fromInt(item.contributions))
    )
  }

  given Decoder[Contributor] = new Decoder[Contributor] {
    final def apply(c: HCursor): Decoder.Result[Contributor] =
      for {
        name <- c.downField("login").as[String]
        contributions <- c.downField("contributions").as[Int]
      } yield Contributor(name, contributions)
  }

  given[F[_]: Applicative]: EntityEncoder[F, Contributor] = circe.jsonEncoderOf[Contributor]
  given[F[_]: Concurrent] : EntityDecoder[F, Contributor] = circe.jsonOf[F, Contributor]


  case class Summary(repositories: Int, contributors: Int, contributions: Int)

  given Encoder[Summary] = new Encoder[Summary] {
    final def apply(item: Summary): Json = Json.obj(
      ("repositories"  -> Json.fromInt(item.repositories)),
      ("contributors"  -> Json.fromInt(item.contributors)),
      ("contributions" -> Json.fromInt(item.contributions))
    )
  }

  given [F[_]: Applicative]: EntityEncoder[F, Summary] = circe.jsonEncoderOf[Summary]
}