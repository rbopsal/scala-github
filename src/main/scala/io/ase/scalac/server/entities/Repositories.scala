///
// Copyright 2024 by Adron Systems LLC, Laporte, MN  56461.
// All rights reserved.
//
//   Purpose: Repository definition plus Argonaut conversions.
///

package io.ase.scalac.server.entities

import cats.effect.Concurrent
import cats.implicits.*
import io.circe.*
import org.http4s.{EntityDecoder, circe}


object Repositories {

  case class Repository(name: Option[String], fullName: String, description: Option[String])

  given Encoder[Repository] = new Encoder[Repository] {
    final def apply(item: Repository): Json = Json.obj(
      ("name" -> item.name.fold(Json.Null)(Json.fromString(_))),
      ("full_name" -> Json.fromString(item.fullName)),
      ("description" -> item.description.fold(Json.Null)(Json.fromString(_)))
    )
  }

  given Decoder[Repository] = new Decoder[Repository] {
    final def apply(c: HCursor): Decoder.Result[Repository] = 
      for {
        name <- c.downField("name").as[Option[String]]
        fullName <- c.downField("full_name").as[String]
        description <- c.downField("description").as[Option[String]]
      } yield Repository(name, fullName, description)
  }

  given[F[_]: Concurrent] : EntityDecoder[F, Repository] = circe.jsonOf[F, Repository]
}