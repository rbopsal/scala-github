///
// Copyright 2024 by Adron Systems LLC, Laporte, MN  56461.
// All rights reserved.
//
//   Purpose: Routes processing for GitHub API server.
///


package io.ase.scalac.server.resources

import io.ase.scalac.server.services.GitHubServices
import cats.Monad
import cats.effect.Concurrent
import cats.implicits.*
import io.circe.*
import io.circe.syntax.EncoderOps
import org.http4s.*
import org.http4s.dsl.Http4sDsl
import org.http4s.dsl.io.OptionalQueryParamDecoderMatcher
import org.http4s.headers.{`Content-Length`, `Content-Type`}

import scala.annotation.targetName

trait GitHubResources[F[_]: Monad: Concurrent] {

  // Access to data services through this service.
  def service: GitHubServices[F]
  
  private given Conversion[Entity[F], Response[F]] = { entity =>
    Response(
      entity = entity,
      headers = Headers(
        `Content-Type`(MediaType.application.json, Charset.`UTF-8`),
        `Content-Length`.fromLong(entity.length.get)
      )
    )
  }

  @targetName("Json encode list T as Response")
  given [T: Encoder]: Conversion[Either[Response[F], List[T]], Response[F]] = {
    case Left(response) => response
    case Right(list: List[T]) => Entity.utf8String(list.asJson.toString)
  }

  @targetName("Json encode item T as Response")
  given [T: Encoder]: Conversion[Either[Response[F], T], Response[F]] = {
    case Left(response) => response
    case Right(item: T) => Entity.utf8String(item.asJson.toString)
  }

  @targetName("Unwrap `Either` to `Response[F]`.")
  given [T: Encoder]: Conversion[F[Either[Response[F], T]], F[Response[F]]] = _ map {
    case Left(response) => response
    case Right(item: T) => Entity.utf8String(item.asJson.toString)
  }

  // Routine for stripping quotes off of string.
  private val stripQuotesRegex = """^[',"]*|[',"]*$""".r
  extension (quotedName: Option[String]) def stripQuotes: Option[String] = {
    quotedName map (stripQuotesRegex.replaceAllIn(_, ""))
  }

  object OptStartsWithMatcher  extends OptionalQueryParamDecoderMatcher[String]("startsWith")
  object OptGreaterThanMatcher extends OptionalQueryParamDecoderMatcher[Int]("greaterThan")

  val pathPrefix = "org"
  def routes: HttpRoutes[F] = {
    val dsl = Http4sDsl[F]
    import dsl.*
    HttpRoutes.of[F] {
      case GET -> Root / pathPrefix / orgName / "contributors" :? OptStartsWithMatcher(osw) +& OptGreaterThanMatcher(ogt) =>
        service.contributors.getOrganizationContributors(orgName, osw.stripQuotes, ogt)

      case GET -> Root / pathPrefix / orgName / ("repos" | "repositories") =>
        service.contributors.getOrganizationRepositories(orgName)

      case GET -> Root / pathPrefix / orgName / "summary" =>
        service.contributors.getOrganizationSummary(orgName)
    }
  }
}