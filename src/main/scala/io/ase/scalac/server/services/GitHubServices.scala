///
// Copyright 2024 by Adron Systems LLC, Laporte, MN  56461.
// All rights reserved.
//
//   Purpose: Services and routes needed for GitHub Api Server.
///

package io.ase.scalac.server.services

import io.ase.scalac.server.resources.GitHubResources
import org.http4s.client.Client

open class GitHubServices[F[_]: cats.effect.Async: cats.Parallel](client: Client[F]) { self =>

  val contributors = new ContributorService[F](client)

  // Overall routes needed by GitHub API server.
  val ghRoutes = new GitHubResources[F] {
    val service = self
  }
}