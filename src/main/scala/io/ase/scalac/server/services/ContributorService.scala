///
// Copyright 2024 by Adron Systems LLC, Laporte, MN  56461.
// All rights reserved.
//
//   Purpose: Client http service to access GitHub contributors.
///

package io.ase.scalac.server.services

import io.ase.scalac.server.entities.Contributors.{*, given}
import io.ase.scalac.server.entities.Repositories.{*, given}
import _root_.scodec.bits.ByteVector
import cats.Parallel
import cats.effect.Concurrent
import cats.implicits.*
import cats.syntax.ApplicativeIdOps
import fs2.Pure
import io.circe.*
import org.http4s.*
import org.http4s.client.Client
import org.http4s.headers.{Accept, Authorization, `Content-Type`}

import java.text.Collator
import java.util.Locale
import scala.annotation.targetName


object ContributorService {
  private val bigPagination = true
  private val maxPerPage = if (bigPagination) 100 else 20

  // Authorization usage for GITHUB_TOKEN and GH_TOKEN environment variable and more.
  private lazy val ghToken = java.lang.System.getenv("GH_TOKEN")
  private lazy val headerAuthorization = Authorization(BasicCredentials("GITHUB_TOKEN", ghToken))
  private lazy val headerAcceptJson = Accept(mediaTypeJson)
  private lazy val mediaTypeJson = MediaType.application.json

  // Compares characters without regard to case or accent marks.
  private val usCollator = Collator.getInstance(Locale.US)
  usCollator.setStrength(Collator.PRIMARY)
  extension (l: String) @targetName("lessThan") infix def `<` (r: String): Boolean = 0 > usCollator.compare(l, r)

  // Sort by descending contributions and, on ties, alphabetically by name.
  val sortContributors: (Contributor, Contributor) => Boolean = {
    case (l, r) if l.contributions >  r.contributions => true
    case (l, r) if l.contributions == r.contributions => l.name `<` r.name
    case _ => false
  }

  // Generate sorted list of contributors from the user/contributions map.
  extension (contributors: Map[String, Int]) def toContributorsSorted: List[Contributor] = {
    contributors.toList map (kv => Contributor(kv(0), kv(1))) sortWith sortContributors
  }

  // Generate mapping between contributors and their total contributions.
  extension (contributors: List[Contributor]) def toContributorMap: Map[String, Int] = {
    contributors.groupMapReduce(_.name)(_.contributions)(_ + _)
  }

  // Combine the two above functions.
  extension (contributors: List[Contributor]) def toContributorSortedList: List[Contributor] = {
    (toContributorMap andThen toContributorsSorted)(contributors)
  }

  extension (contributors: List[Contributor]) def contributionsGreaterThan(ogt: Option[Int]): List[Contributor] = {
    ogt.fold(contributors)(gt => contributors.filter(_.contributions > gt))
  }

  extension (contributors: List[Contributor]) def contributorsStartingWith(osw: Option[String]): List[Contributor] = {
    osw match {
      case None => contributors
      case Some(sw) if sw.isBlank => contributors
      case Some(sw) => contributors.filter { contributor =>
        usCollator.equals( contributor.name.take(sw.length), sw )
      }
    }
  }
}


class ContributorService[F[_]: Concurrent: Parallel](client: Client[F]) {

  import ContributorService.*

  private def getResponseAsJson(uri: String): F[Either[Response[F], Json]] = {

    extension (headers: Headers) def containsMediaTypeJson: Boolean = {
      headers.get[`Content-Type`].fold(false)(_.mediaType == mediaTypeJson)
    }

    val getHttpJsonRequest: String => Request[F] = uri => {
      
      Request[F](
        method  = Method.GET,
        uri     = Uri.unsafeFromString(uri),
        headers = Headers(headerAuthorization, headerAcceptJson)
      )
    }

    // Use the `Resource-Use` mechanism for obtaining a strict (pure) Http4s response.
    val getHttpResponsePure: Request[F] => F[Response[Pure]] = request => {

      given toStrict: Conversion[Response[F], F[Response[Pure]]] = { response =>
        response.body.compile.to(ByteVector) map (bv => response.copy(entity = Entity.Strict(bv)))
      }

      // Json value to `pure` entity as base64 string.
      client.run(request).use(toStrict)
    }

    val processHttpResponse: F[Response[Pure]] => F[Either[Response[F], Json]] = responsePure => {

      for {
        response <- responsePure
        status  = response.status
        headers = response.headers
        body    = response.body
        fault   = Left(Response(status = Status.UnprocessableEntity))
      } yield {
        status match {
          // ByteVector from base64 encoding to utf8 and then `parse` result to Json.
          case Status.Ok if headers.containsMediaTypeJson =>
            body.to(ByteVector).decodeUtf8.fold(_ => fault, utf8 =>
              io.circe.parser.parse(utf8).fold(_ => fault, Right(_))
            )
          case _ => Left(response)
        }
      }
    }

    (getHttpJsonRequest andThen getHttpResponsePure andThen processHttpResponse)(uri)
  }

  // Recursive handling of pagination.
  private def getResponseList[T: Decoder](uri: String): F[Either[Response[F], List[T]]] = {

    extension [T](fAcc: F[Either[Response[F], List[T]]]) @targetName("concat")
    infix def `:::` (listAdd: List[T]): F[Either[Response[F], List[T]]] = {

      fAcc map {
        case response@Left(_) => response
        case Right(listAcc) => Right(listAcc ::: listAdd)
      }
    }

    val emptyList: F[Either[Response[F], List[T]]] = ApplicativeIdOps(Right(List.empty[T])).pure[F]
    def getResponseListR(page: Int = 1, accumulate: F[Either[Response[F], List[T]]] = emptyList)
      : F[Either[Response[F], List[T]]] = {

      val getPageOfItems: Int => F[Either[Response[F], List[T]]] = page => {
        val uriWithPage = s"$uri?per_page=$maxPerPage;page=$page"
        getResponseAsJson(uriWithPage) map {
          case Left(response) => Left(response)
          case Right(json) =>
            json.as[List[T]] match {
              case Left(_) => Right(List.empty[T])
              case Right(items) => Right(items)
            }
        }
      }

      val collateItemsPage: F[Either[Response[F], List[T]]] => F[Either[Response[F], List[T]]] = fEitherIn => {

        fEitherIn flatMap {
          case response@Left(_) => ApplicativeIdOps(response).pure[F]
          case Right(listNext) => accumulate flatMap {
            case response@Left(_) => ApplicativeIdOps(response).pure[F]
            case Right(listAcc) if listNext.length < maxPerPage => ApplicativeIdOps(Right(listAcc ::: listNext)).pure[F]
            case Right(_) => getResponseListR(page + 1, accumulate `:::` listNext)
          }
        }
      }

      (getPageOfItems andThen collateItemsPage)(page)
    }

    getResponseListR()
  }

  // Per repository, return each individual's contributions.
  private def getRepositoryContributors(fullRepName: String): F[Either[Response[F], List[Contributor]]] = {

    val uriRepositoryContributors = s"https://api.github.com/repos/$fullRepName/contributors"
    getResponseList[Contributor](uriRepositoryContributors)
  }

  // Per repository, return each individual's contributions using parallel traverse.
  // Faster than the sequential version with simple `traverse` by factor of 3x to 4x.
  private def getAllRepositoryContributorsPar(repositories: List[Repository]): F[Either[Response[F], List[Contributor]]] = {

    val initialState: Either[Response[F], List[Contributor]] = Right(List.empty[Contributor])
    repositories.parTraverse(repository => getRepositoryContributors(repository.fullName)) map {
      _.fold(initialState) { (acc, next) =>
        acc match {
          case response@Left(_) => response
          case Right(listAcc) =>
            next match {
              case response@Left(_) => response
              case Right(listNext) => Right(listAcc ::: listNext)
            }
        }
      }
    }
  }

  // Return the complete listing of organization repositories.
  def getOrganizationRepositories(orgName: String): F[Either[Response[F], List[Repository]]] = {

    val uriOrganizationRepositories = s"https://api.github.com/orgs/${orgName}/repos"
    getResponseList[Repository](uriOrganizationRepositories)
  }

  // Return individual user contributions to all the organization's repositories.
  def getOrganizationContributors(orgName: String, osw: Option[String], ogt: Option[Int])
    : F[Either[Response[F], List[Contributor]]] = {

    for {
      maybeRepositories <- getOrganizationRepositories(orgName)
      repositories       = maybeRepositories.getOrElse(Nil)
      maybeContributors <- getAllRepositoryContributorsPar(repositories)
      contributors       = maybeContributors.getOrElse(Nil)
    } yield Right(contributors
      .toContributorSortedList
      .contributorsStartingWith(osw)
      .contributionsGreaterThan(ogt)
    )
  }

  // Return summary with repository count, contributor count, and total contributions.
  def getOrganizationSummary(orgName: String): F[Either[Response[F], Summary]] = {

    for {
      maybeRepositories <- getOrganizationRepositories(orgName)
      repositories       = maybeRepositories.getOrElse(Nil)
      maybeContributors <- getAllRepositoryContributorsPar(repositories)
      contributors       = maybeContributors.getOrElse(Nil)
      contributorMap     = contributors.toContributorMap
    } yield Right(Summary(
      repositories  = repositories.length,
      contributors  = contributorMap.size,
      contributions = contributorMap.foldLeft(0)((acc, kv) => kv(1) + acc)
    ))
  }
}