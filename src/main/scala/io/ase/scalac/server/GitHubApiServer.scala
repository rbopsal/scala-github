///
// Copyright 2024 by Adron Systems LLC, Laporte, MN  56461.
// All rights reserved.
//
//   Purpose: Entrance for the GitHub API server.
///

package io.ase.scalac.server

import io.ase.scalac.server.services.GitHubServices
import cats.Parallel
import com.comcast.ip4s.*
import com.typesafe.config.ConfigFactory
import fs2.io.net.Network
import org.http4s.ember.client.EmberClientBuilder
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.implicits.*
import org.http4s.server.middleware.Logger
import org.typelevel.log4cats.LoggerFactory
import org.typelevel.log4cats.slf4j.Slf4jFactory
import zio.*
import zio.interop.catz.*
import zio.logging.backend.SLF4J

import java.io.File


///
// The GitHub Http server object with entrance method.
///

object GitHubApiServer extends ZIOAppDefault {
//object GitHubApiServer extends cats.effect.IOApp.Simple{

  // See https://github.com/lightbend/config
  // Priority loads `application.conf` from distribution `resources` folder.
  // Fallback loads `application.conf` from the application's classpath.
  private val appConf = "/resources/application.conf"
  private val config = ConfigFactory.parseFileAnySyntax(new File("." + appConf))
    .withFallback(ConfigFactory.parseFileAnySyntax(new File(".." + appConf)))
    .withFallback(ConfigFactory.load())
    .resolve()

  private val host = config.getString("githubserver.host")
  private val port = config.getInt("githubserver.port")

  // Comment out ZLayer when running cats.effect.IO.
  // Override ZIO default logger.
  override val bootstrap: ZLayer[ZIOAppArgs, Any, Any] = Runtime.removeDefaultLoggers >>> SLF4J.slf4j

  // Shared logging for services and routes.
  given[F[_]: cats.effect.Sync]: LoggerFactory[F] = Slf4jFactory.create[F]

  def server[F[_]: cats.effect.Async: Network: Parallel] = for {
    client  <- EmberClientBuilder.default[F].build

    services     = new GitHubServices[F](client)
    httpApp      = (services.ghRoutes.routes).orNotFound
    finalHttpApp = Logger.httpApp(true, true)(httpApp)

    server <- EmberServerBuilder
      .default[F]
      .withHostOption(Host.fromString(host))
      .withPort(Port.fromInt(port).getOrElse(port"9090"))
      .withHttpApp(finalHttpApp)
      .withShutdownTimeout(scala.concurrent.duration.FiniteDuration(0, "s"))
      .build
    } yield server

  def run: ZIO[Scope, Throwable, Unit] = for {
    _ <- Scope.addFinalizer(ZIO.logInfo("Finalize initial scope"))
    _ <- server[Task].toScopedZIO
    _ <- Scope.addFinalizer(ZIO.logInfo("Finalize server scope"))
    _ <- ZIO.never
  } yield ()

//  def run: cats.effect.IO[Nothing] = {
//    for {
//      _ <- server[cats.effect.IO]
//    } yield ()
//  }.useForever
}