import NativePackagerHelper._

ThisBuild / organization := "io.ase"
ThisBuild / scalaVersion := "3.5.2"
ThisBuild / scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-unchecked",
  "-explain",
  "-feature",
  "-no-indent",
  "-source:future",
  "-language:implicitConversions",
  "-language:postfixOps"
)

lazy val githubserver = (project in file("."))
  .enablePlugins(JavaAppPackaging)
  .enablePlugins(UniversalPlugin)
  .settings(
    name       := Settings.name,
    maintainer := Settings.maintainer,
    version    := Settings.version,
    organizationName := Settings.organizationName,
    packageDescription := Settings.packageDescription,
    libraryDependencies ++= Seq(

      Dependencies.scodecCore,

      Dependencies.cats,
      Dependencies.catsSlf4j,

      Dependencies.circeCore,
      Dependencies.circeGeneric,
      Dependencies.circeLiteral,
      Dependencies.circeParser,
      Dependencies.circeScodec,

      Dependencies.http4sCirce,
      Dependencies.http4sDsl,
      Dependencies.http4sEmberClient,
      Dependencies.http4sEmberServer,

      Dependencies.zio,
      Dependencies.zioCats,
      Dependencies.zioLogging,

      Dependencies.config,
      Dependencies.jansi,
      Dependencies.slf4j,
      Dependencies.logbackClassic
    )
  )

// For Universal packaging via sbt-native-packager.
Universal / mappings ++= directory(sourceDirectory.value / "main" / "resources")