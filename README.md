#  GitHub API Server Notes

## Scala, Akka-Http, ZIO Http & Http4s

The GitHub API Server demonstration program was written with the [Scala 3](https://www.scala-lang.org/) programming language.  Three versions have been produced utilizing the following server / client frameworks:

* [Akka-Http](https://doc.akka.io/docs/akka-http/current/introduction.html)
* [ZIO Http](https://zio.dev/zio-http/)
* [Http4s](https://http4s.org/)

These are stored as branches in a [BitBucket](https://bitbucket.org/rbopsal/scala-github/src/ZIO_http/) repository. The `main` branch corresponds to Akka-Http.

The branches were produced in the above order, converting from one branch to the next.

## Execution

To execute the GitHub API Server from Jetbrain's [IntelliJ](https://www.jetbrains.com/idea/), locate the `GitHubApiServer.scala` file and run one of the following:

* `GitHubApiServer` object
* `@main def startGitHubApiServer()` function

From a browser, [Insominia](https://insomnia.rest/), or [Postman](https://www.postman.com/), call the GitHub API endpoint utilizing the following uri:

```
http://localhost:9090/org/scala/contributors
http://localhost:9090/org/scala/contributors?startsWith=ol
http://localhost:9090/org/scala/contributors?greaterThan=1000
http://localhost:9090/org/scala/summary
```
In this example, the `scala` organization can be replaced with another organization value, such as `lampepfl`.

The optional `startsWith` query returns contributors whose name starts with the passed value.  The name prefix value can optionally be quoted.

The optional `greaterThan` query returns contributors whose contributions are greater than the passed value.

The first endpoint generates a JSON listing, by contributor and contributions, for all repositories within the specified organization.  In this example, `scala` was the organization queried.  Here is an example of the JSON produced:

```json
[
    {
        "name": "odersky",
        "contributions": 14758
    },
    {
        "name": "SethTisue",
        "contributions": 12534
    },
    .
    .
    .
]    
```

The last endpoint generates a simple summary of the organization's contributions.  Here is an example of the JSON produced:

```json
{
    "repositories": 59,
    "contributors": 2462,
    "contributions": 102920
}
```

## Aspects

The interesting code for this project is in the `ContributorService.scala` file.  The functions in this file do the work of querying GitHub for:

* [Organization repositories](https://docs.github.com/en/rest/reference/repos#list-organization-repositories).
* [Repository contributors](https://docs.github.com/en/rest/reference/repos#list-repository-contributors).

While programming these branches, focus on these aspects:

* Effectual, functional programming for the ZIO Http and Http4s versions.
* Utilize Scala 3 aspects, such as:
	* Given conversions
	* Extension methods
	* Polymorphic functions

The `ContributorService.scala` file has these features:

* Queries are broken into pages and collated.
* Results are sorted by descending contributions and, on ties, by contributor name.

In the `ContributorService.scala` file, there are constants that can alter the running of the API server:

```scala
object ContributorService {
  private val bigPagination = true
  private val maxPerPage = if (bigPagination) 100 else 20
}
```

* The `bigPagination` boolean specifies whether page results should be gathered in small or big increments.
* `maxPerPage` alters the number of records returned for each query, with 100 being the maximum allowed value.  The GitHub default value is 30.

For ZIO Http, the `getAllRepositoryContributorsPar()` function incorporates `ZIO.foreachPar`.  This parallelized version sped up processing time by, roughly, a factor of three compared to the sequential version.

For Http4s, the `getAllRepositoryContributorsPar()` function incorporates `Parallel#parTraverse`.  This sped up processing time by a factor of three to four compared to the sequential version.

For the Http4s version, the code can be run using either the Cats effect or ZIO runtime.  Most of the code base was written in [tagless final](https://www.baeldung.com/scala/tagless-final-pattern) form, allowing easy transition between runtimes.  The `GitHubApiServer.scala` file specifies either `cats.effect.IO` or `ZIO#Task` runtime.

## Development

The GitHub API Server was developed utilizing JetBrains' [IntelliJ](https://www.jetbrains.com/idea/) IDE.  The GitHub API Server can run directly within the Intellij IDE and is recommended for development.

Amazon's Corretto [Java 21.0.4.7.1 JDK](https://corretto.aws/downloads/resources/21.0.4.7.1/amazon-corretto-21.0.4.7.1-windows-x64.msi) was utilized for final development.

[Scala 3.4.2](https://www.scala-lang.org/) was utilized for the GitHub API Server, along with recent versions of the server / client frameworks.

## Environment - GH_TOKEN

The environment variable, GH_TOKEN, should be set to a GitHub Personal Access Token.  For further information, please see [Using Personal Access Tokens with GIT and GitHub](https://www.edgoad.com/2021/02/using-personal-access-tokens-with-git-and-github.html).  This token is utilized in the `ContributorService.scala` file.

## Environment - application.conf

Scala projects can have an `application.conf` configuration file.  For the GitHub API Server, you can alter these settings:

```
# Http(s) server configuration.
githubserver {
  host = "localhost"
  port = 9090
}
```

* `host`: Port 9090 was utilized during development.  This can be modified to port 8080.
* `port`: The API Server has only been deployed on a local machine.

## Deployment

The GitHub API Server `plugins.sbt` file contains the [SBT Native Packager](https://github.com/sbt/sbt-native-packager).  This is the recommended route for generating a distributable package.  From the project root folder, issue the command:

```
sbt.bat universal:packageBin
```

From the project root folder, go to the `target\universal` directory.  If all went well, you should find a zip file, such as:

```
github-server-1.0.0.zip
```

This is the server distributable package.  Unpack (unzip) to a select folder.  Then, on Windows, run from the root:

```
bin\github-server.bat
```

or on Linux, run:

```
bin/github-server
```

**Note:** The GitHub API Server gives priority to loading `application.conf` from the `resources` directory rather than `application.conf` embedded within the application's jar file.  The GitHub API Server is mimicking the following command:

```
bin\github-server.bat -Dconfig.file=resources/application.conf
```

## Thoughts

The three branches run at roughly the same speed without the parallelization enhancements for the ZIO versions.  

Based on prior experience, Akka-Http has proven to be a solid server foundation.  It's routing DSL seems a bit verbose but works well.  Akka-Http has extensive and well-written documentation, which is greatly appreciated.  The documentation has good code snippets.

Regarding ZIO Http and Http4s, Http4s has a longer history, better documentation, and its routing DSL is fairly intuitive.  Http4s makes extensive use of FS2 streams. This made writing the Http4s branch more difficult due to a lack of experience on the author's part.  

For lighter projects that utilize ZIO, ZIO Http is probably the best route, especially if learning the ZIO architecture is a goal. Note that the ZIO Http project is still in the early development stage.

For more serious projects that wish to utilize ZIO, this author would be inclined to utilize Http4s.