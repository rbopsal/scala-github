import sbt._

object Settings {
  val name = "github-server"
  val version = "1.0.0"
  val maintainer = "Richard Opsal <ropsal@adronsystems.com>"
  val organizationName = "Adron Systems LLC"
  val packageDescription = "GitHub API Server"
}