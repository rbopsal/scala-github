logLevel := Level.Warn

// See GitHub -> https://github.com/sbt/sbt-native-packager
addSbtPlugin("com.github.sbt" % "sbt-native-packager" % "1.9.16")