import sbt._

object Dependencies {
  object Version {
    val cats          = "2.12.0"
    val catsEffect    = "3.5.4"
    val catsSlf4j     = "2.7.0"
    val circe         = "0.14.10"
    val http4s        = "1.0.0-M43"
    val scodec        = "2.3.2"
    val zio           = "2.1.12"
    val zioCats       = "23.1.0.3"
    val zioLogging    = "2.3.1"
    val config        = "1.4.3"
    val jansi         = "2.4.1"
    val logback       = "1.5.12"
    val slf4j         = "2.0.16"
  }

  val cats              = "org.typelevel"     %% "cats-core"            % Version.cats
  val catsEffect        = "org.typelevel"     %% "cats-effect"          % Version.catsEffect
  val catsSlf4j         = "org.typelevel"     %% "log4cats-slf4j"       % Version.catsSlf4j

  val circeCore         = "io.circe"          %% "circe-core"           % Version.circe
  val circeGeneric      = "io.circe"          %% "circe-generic"        % Version.circe
  val circeLiteral      = "io.circe"          %% "circe-literal"        % Version.circe
  val circeParser       = "io.circe"          %% "circe-parser"         % Version.circe
  val circeScodec       = "io.circe"          %% "circe-scodec"         % Version.circe

  val http4sCirce       = "org.http4s"        %% "http4s-circe"         % Version.http4s
  val http4sDsl         = "org.http4s"        %% "http4s-dsl"           % Version.http4s
  val http4sEmberClient = "org.http4s"        %% "http4s-ember-client"  % Version.http4s
  val http4sEmberServer = "org.http4s"        %% "http4s-ember-server"  % Version.http4s

  val scodecCore        = "org.scodec"        %% "scodec-core"          % Version.scodec

  val zio               = "dev.zio"           %% "zio"                  % Version.zio
  val zioCats           = "dev.zio"           %% "zio-interop-cats"     % Version.zioCats
  val zioLogging        = "dev.zio"           %% "zio-logging-slf4j2"   % Version.zioLogging

  // These are Java libraries and are independent of Scala.
  val config         = "com.typesafe"         % "config"                % Version.config
  val jansi          = "org.fusesource.jansi" % "jansi"                 % Version.jansi
  val logbackClassic = "ch.qos.logback"       % "logback-classic"       % Version.logback
  val slf4j          = "org.slf4j"            % "slf4j-api"             % Version.slf4j
}